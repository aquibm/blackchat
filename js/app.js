var blackChat = new angular.module('BlackChat', ['firebase']);

var mlg = [
    'GLBHxgnYm8c',
    'XmH9Qd1WAPU',
    'AAzL13OS0vM',
    'ROlMmQU5yDg',
    'SSxqjhl0QeY',
    '9m5c_XUiig0',
    'Y5WC6hXPvVM',
    'qKX-aI1TQXM',
    'PfUK1lLAUW4',
    'THC2TsvG4Dce',
    'Q5Bb6LKyHZI',
    '08FVYFeEdEc',
    '-h7GL5K4jaY',
    '-od1XPZEcqA',
    'SzvB-Fhy3rI',
    'u63loMxSGZA',
    'jbLo2_KnXRY',
    'j4r_TYeAsCs',
    'Nwfujr-oMjE',
    '1TgZ68HkbxE',
    'CZHQSjyKuOs'
];

blackChat.controller('ChatController', function($scope, $firebase) {
    var ref = new Firebase('https://keebangular.firebaseio.com/blackchat');
    var chatText = $firebase(ref);

    this.message = chatText.$asObject();

    this.sendMessage = function() {
        chatText.$set('message', this.newMessage);

        this.newMessage = '';
        this.inputChanged();
    };

    this.message.$watch(function() {
        var msg = chatText.$asObject().message;

        if(msg.toLowerCase().indexOf('mlg') != -1) {
            var randItem = mlg[Math.floor(Math.random() * mlg.length)];
            $('#YoutubeFrame').attr('src', '//www.youtube.com/embed/' + randItem + '?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1');
        } else if(msg.toLowerCase().indexOf('rickroll') != -1) {
            $('#YoutubeFrame').attr('src', '//www.youtube.com/embed/dQw4w9WgXcQ?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1');
        } else if(msg.toLowerCase().indexOf('wasnt me') != -1) {
            $('#YoutubeFrame').attr('src', '//www.youtube.com/embed/2g5Hz17C4is?rel=0&amp;controls=0&amp;showinfo=0&amp;autoplay=1&amp;iv_load_policy=3');
        } else if(msg.toLowerCase().indexOf('slap') != -1) {
            $('#YoutubeFrame').attr('src', 'https://38.media.tumblr.com/f73fa94878fbae29805b393a92d682b7/tumblr_n1dqduh12F1qdlh1io1_400.gif');
        } else if(msg.toLowerCase().indexOf('clearvid') != -1) {
            $('#YoutubeFrame').attr('src', '');
        }
    });

    this.inputChanged = function() {
        if(this.newMessage.length > 0) {
            chatText.$set('typing', '1');
        } else {
            chatText.$set('typing', '0');
        }
    };

});